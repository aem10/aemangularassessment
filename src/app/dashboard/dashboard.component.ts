import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import * as d3 from 'd3';
import { chartBarModel } from '../model/chartBar.model';
import { chartDonutModel } from '../model/chartDonut.model';
import { tableUserModel } from '../model/tableUser.model';
import { D3Service } from '../services/d3.service';
import { url } from '../shared/constant';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent {
  isLoading: boolean = false
  tableUser: tableUserModel[] = []
  chartDonutData: chartDonutModel[] = []
  chartBarData: chartBarModel[] = []

  private svg: any;
  private margin = 28;
  private width = 580;
  private height = 535;

  private donutMargin = { top: 10, right: 60, bottom: 30, left: 60 };
  private donutWidth = 600;
  private donutHeight = 600;
  private donutSvg: any;
  private colors: any;
  private radius = Math.min(this.donutWidth, this.donutHeight) / 2 - this.donutMargin.left;

  constructor(private d3: D3Service,private router: Router) { }

  ngOnInit(): void {
    if(!localStorage.getItem('bearerToken')){
      this.router.navigateByUrl('/login')
    } else{
      console.log(localStorage.getItem('bearerToken'))
    }
    
    this.getDashboardData()
  }

  getDashboardData() {
    this.isLoading = false

    fetch(url + 'api/dashboard', {
      method: 'get',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('bearerToken'), // notice the Bearer before your token
      },
    }).then((response) => {
      if (!response.ok) {
        response.json().then(x => {
          console.log(x)
          this.isLoading = false
        })
        // throw response;
        return;
      }

      response.json().then(x => {
        this.tableUser = tableUserModel.fromArrayJS(x['tableUsers'])
        this.chartDonutData = chartDonutModel.fromArrayJS(x['chartDonut'])
        this.chartBarData = chartBarModel.fromArrayJS(x['chartBar'])

        if (this.chartBarData.length != 0) {
          this.createSvg();
          this.drawBars(this.chartBarData);
        }

        if (this.chartDonutData.length != 0) {
          console.log(this.chartDonutData)
          this.createDonutSVG();
          this.createColors(this.chartDonutData);
          this.drawChart();
        }

        this.isLoading = false
      })
    })
  }

  signout(){
    localStorage.clear()
    this.router.navigateByUrl('/login')
  }

  // 
  // Bar chart 
  //

  private createSvg(): void {
    this.svg = d3.select("figure#bar")
      .append("svg")
      .attr("width", this.width + (this.margin * 2))
      .attr("height", this.height + (this.margin * 2))
      .append("g")
      .attr("transform", "translate(" + this.margin + "," + this.margin + ")");
  }


  private drawBars(data: any[]): void {
    // Create the X-axis band scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(data.map(d => d.name))
      .padding(0.2);

    // Draw the X-axis on the DOM
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Create the Y-axis band scale
    const y = d3.scaleLinear()
      .domain([0, 100])
      .range([this.height, 0]);

    // Draw the Y-axis on the DOM
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill the bars
    this.svg.selectAll("bars")
      .data(data)
      .enter()
      .append("rect")
      .attr("x", (d: any) => x(d.name))
      .attr("y", (d: any) => y(d.value))
      .attr("width", x.bandwidth())
      .attr("height", (d: any) => this.height - y(d.value))
      .attr("fill", "#d04a35");
  }


  // 
  // donut chart 
  // 

  private createDonutSVG(): void {
    this.donutSvg = this.d3.d3
      .select("figure#donut")
      .append("svg")
      .attr("viewBox", `0 0 ${this.donutWidth} ${this.donutHeight}`)
      .append("g")
      .attr(
        "transform",
        "translate(" + this.donutWidth / 2 + "," + this.donutHeight / 2 + ")"
      );
  }

  private createColors(data : any): void {
    let index = 0;
    const defaultColors = [
      "#6773f1",
      "#32325d",
      "#6162b5",
      "#6586f6",
      "#8b6ced",
      "#1b1b1b",
      "#212121"
    ];
    const colorsRange : any = [];
    this.chartDonutData.forEach(element => {

        colorsRange.push(defaultColors[index]);
        index++;
      
    });
    this.colors = this.d3.d3
      .scaleOrdinal()
      .domain(data.map((d:any) => d.value.toString()))
      .range(colorsRange);
  }

  private drawChart(): void {
    // Compute the position of each group on the pie:
    var pie = this.d3.d3
      .pie()
      .sort(null) // Do not sort group by size
      .value((d:any) => {
        return d.value;
      });

    let data : any[] = this.chartDonutData
    var data_ready = pie(data);

    // The arc generator
    var arc = this.d3.d3
      .arc()
      .innerRadius(this.radius * 0.5) // This is the size of the donut hole
      .outerRadius(this.radius * 0.8);

    // Another arc that won't be drawn. Just for labels positioning
    var outerArc = this.d3.d3
      .arc()
      .innerRadius(this.radius * 0.7)
      .outerRadius(this.radius * 0.7);

    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    this.donutSvg
      .selectAll("allSlices")
      .data(data_ready)
      .enter()
      .append("path")
      .attr("d", arc)
      .attr("fill", (d:any) => this.colors(d.data.value))
      .attr("stroke", "white")
      .style("stroke-width", "2px")
      .style("opacity", 0.7);

    // Add the polylines between chart and labels:
    this.donutSvg
      .selectAll("allPolylines")
      .data(data_ready)
      .enter()
      .append("polyline")
      .attr("stroke", "black")
      .style("fill", "none")
      .attr("stroke-width", 1)
      .attr("points", (d:any) => {
        var posA = arc.centroid(d); // line insertion in the slice
        var posB = outerArc.centroid(d); // line break: we use the other arc generator that has been built only for that
        var posC = outerArc.centroid(d); // Label position = almost the same as posB
        var midangle = d.startAngle + (d.endAngle - d.startAngle) / 2; // we need the angle to see if the X position will be at the extreme right or extreme left
        posC[0] = this.radius * 0.95 * (midangle < Math.PI ? 1 : -1); // multiply by 1 or -1 to put it on the right or on the left
        return [posA, posB, posC];
      });

    // Add the polylines between chart and labels:
    this.donutSvg
      .selectAll("allLabels")
      .data(data_ready)
      .enter()
      .append("text")
      .text((d:any) => {
        return d.data.name;
      })
      .attr("transform", (d:any) => {
        var pos = outerArc.centroid(d);
        var midangle = d.startAngle + (d.endAngle - d.startAngle) / 2;
        pos[0] = this.radius * 0.99 * (midangle < Math.PI ? 1 : -1);
        return "translate(" + pos + ")";
      })
      .style("text-anchor", (d:any) => {
        var midangle = d.startAngle + (d.endAngle - d.startAngle) / 2;
        return midangle < Math.PI ? "start" : "end";
      });
  }

}

