import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { url } from '../shared/constant';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})

export class LoginComponent {
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);
  matcher = new MyErrorStateMatcher();
  // so we will define our constants here
  username: string = ''
  password: string = ''
  isLoading: boolean = false
  UsernameError: string[] = []
  passwordError: string[] = []


  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  // now we configure the fetch request to the url endpoint.
  // we should probably put it inside a separate function since
  // you're using a browser, you probably will bind this request
  // to a click event or something.
  login() {
    this.isLoading = true;
    fetch(url + 'api/account/login', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.username,
        password: this.password,
      }),
    }).then((response) => {
      if (!response.ok) {
        response.json().then(value => {
          this.UsernameError = value['Username']
          this.passwordError = value['Password']
          this.username = "";
          this.password = ""
          this.isLoading = false
        })
        // throw response;
        return;
      }

      response.json().then(x => {
        localStorage.setItem('bearerToken', x)
        this.router.navigateByUrl('/dashboard')
        this.isLoading = false
        
      })

    })
  }
}
