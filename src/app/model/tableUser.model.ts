export class tableUserModel {
    firstName?: string;
    lastName?: string;
    username?: string;

    init(_data?: any) {
        if (_data) {
            this.firstName = _data["firstName"];
            this.lastName = _data["lastName"];
            this.username = _data["username"];
        }
    }

    static fromJS(data: any): tableUserModel {
        data = typeof data === 'object' ? data : {};
        let result = new tableUserModel();
        result.init(data);
        return result;
    }

    static fromArrayJS(data: any): tableUserModel[] {
        let arrayData: any = null;
        if (Array.isArray(data)) {
            arrayData = [] as any;
            for (let item of data)
                arrayData!.push(tableUserModel.fromJS(item));
        } else {
            arrayData = <any>null;
        }

        return arrayData
    }
}