export class chartDonutModel {
    name?: string;
    value?: number;

    init(_data?: any) {
        if (_data) {
            this.name = _data["name"];
            this.value = _data["value"];
        }
    }

    static fromJS(data: any): chartDonutModel {
        data = typeof data === 'object' ? data : {};
        let result = new chartDonutModel();
        result.init(data);
        return result;
    }

    static fromArrayJS(data: any): chartDonutModel[] {
        let arrayData: any = null;
        if (Array.isArray(data)) {
            arrayData = [] as any;
            for (let item of data)
                arrayData!.push(chartDonutModel.fromJS(item));
        } else {
            arrayData = <any>null;
        }

        return arrayData
    }
}